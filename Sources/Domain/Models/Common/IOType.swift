//
//  IOType.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation

public protocol IOType {
  associatedtype Request
  associatedtype Response
}
