//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public struct BaseError: Codable{
  public let code: String
  public let message: String
}
