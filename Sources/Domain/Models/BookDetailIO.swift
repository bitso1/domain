//
//  BookDetailIO.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public enum BookDetailIO: IOType {
  
  public struct Request: Codable {
    public let book: String
    
    public init(bookSymbol: String) {
      self.book = bookSymbol
    }
  }
  
  public struct Response: Codable {
    public let book: String
    public let volume: String
    public let high: String
    public let last: String
    public let low: String
    public let vwap: String
    public let ask: String
    public let bid: String
    public let created_at: Date
  }
}
