//
//  UseCaseProvider.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public protocol UseCaseProvider {
  func getHomeUseCases() -> HomeUseCases
  func getBookDetailsUseCases() -> BookDetailsUseCases
}
