//
//  BookDetailsUseCases.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
public protocol BookDetailsUseCases {
  func getDetails(request: BookDetailIO.Request, completion: @escaping (Result<BookDetailIO.Response, AppError>) -> Void) -> URLSessionDataTask
}
